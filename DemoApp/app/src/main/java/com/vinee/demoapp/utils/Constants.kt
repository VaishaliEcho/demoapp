package com.vinee.demoapp.utils


const val INTENT_300 = 300
const val INTENT_400 = 400


const val DEVICE_TYPE: String = "1"

const val DATE_TIME_CHANGE = "DATE_TIME_CHANGE"
const val SUCCESS : String = "SUCCESS"
const val ID : String = "ID"
const val VALUE : String = "VALUE"
const val MODEL : String = "MODEL"
const val EN: String = "en"

const val BASE_URL: String = "https://yourapp.base.url/"