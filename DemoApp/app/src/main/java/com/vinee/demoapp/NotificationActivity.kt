package com.vinee.demoapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.vinee.demoapp.adapter.NotificationListAdapter
import com.vinee.demoapp.databinding.ActivityNotificationBinding
import com.vinee.demoapp.model.NotificationModel

open class NotificationActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityNotificationBinding
    private lateinit var notificationListAdapter: NotificationListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialization()

    }


    private fun initialization() {
        binding.customHeader.setTitle(resources.getString(R.string.notifications))

        notificationListAdapter = NotificationListAdapter(this)
        binding.rvNotification.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.rvNotification.adapter = notificationListAdapter

        notificationList()


        binding.swipeContainer.setOnRefreshListener { binding.swipeContainer.isRefreshing = false}
    }



    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogin -> {
                startActivity(Intent(this@NotificationActivity, NotificationActivity::class.java))
            }
        }
    }


    private fun notificationList() {
        notificationListAdapter.objList = ArrayList()

        var model = NotificationModel()
        model.message = "Reminder for take Medicine"
        model.created = "24 Dec at 9:00 AM"
        notificationListAdapter.objList.add(model)

        model = NotificationModel()
        model.message = "Please drink water"
        model.created = "28 Jul at 10:03 PM"
        notificationListAdapter.objList.add(model)

        model = NotificationModel()
        model.message = "Reminder for take Medicine"
        model.created = "24 Dec at 9:00 AM"
        notificationListAdapter.objList.add(model)

        model = NotificationModel()
        model.message = "Please drink water"
        model.created = "28 Jul at 10:03 PM"
        notificationListAdapter.objList.add(model)


        model = NotificationModel()
        model.message = "Reminder for take Medicine"
        model.created = "24 Dec at 9:00 AM"
        notificationListAdapter.objList.add(model)

        model = NotificationModel()
        model.message = "Please drink water"
        model.created = "28 Jul at 10:03 PM"
        notificationListAdapter.objList.add(model)


        notificationListAdapter.addData(notificationListAdapter.objList)
    }


}