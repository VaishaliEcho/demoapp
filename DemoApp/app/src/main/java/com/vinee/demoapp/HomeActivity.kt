package com.vinee.demoapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.vinee.demoapp.databinding.ActivityHomeBinding
import com.vinee.demoapp.databinding.ActivityLoginBinding

open class HomeActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialization()

    }


    private fun initialization() {

        binding.customHeader.setTitle(resources.getString(R.string.home))
        binding.customHeader.hideBackButton()
        binding.customHeader.setRightDrawable(R.drawable.ic_notifications)
        binding.customHeader.get().btnRight.setOnClickListener(this)
    }


    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnRight -> {
                startActivity(Intent(this@HomeActivity, NotificationActivity::class.java))
            }
        }
    }

}