package com.vinee.demoapp.utils

import android.content.Context
import android.content.SharedPreferences
import com.vinee.demoapp.MainApplication

object Pref {
    private var sharedPreferences: SharedPreferences? = null
    private fun openPreference() {
        sharedPreferences =
            MainApplication.instance.getSharedPreferences("DEMO_PREF", Context.MODE_PRIVATE)
    }

    /*Fore String Value Store*/
    fun getStringValue(key: String, defaultValue: String): String? {
        openPreference()
        val result = sharedPreferences!!.getString(key, defaultValue)
        sharedPreferences = null
        return result
    }

    fun setStringValue(key: String, value: String) {
        openPreference()
        val prefsPrivateEditor: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsPrivateEditor!!.putString(key, value)
        prefsPrivateEditor.apply()
        sharedPreferences = null
    }


    /*For Integer Value*/

    fun setIntValue(key: String, value: Int) {
        openPreference()
        val prefsPrivateEditor: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsPrivateEditor!!.putInt(key, value)
        prefsPrivateEditor.apply()
        sharedPreferences = null
    }

    fun getIntValue(key: String, defaultValue: Int): Int {
        openPreference()
        val result = sharedPreferences!!.getInt(key, defaultValue)
        sharedPreferences = null
        return result
    }


    /*For boolean Value Store*/

    fun getBooleanValue(key: String, defaultValue: Boolean): Boolean {
        openPreference()
        val result = sharedPreferences!!.getBoolean(key, defaultValue)
        sharedPreferences = null
        return result
    }

    fun setBooleanValue(key: String, value: Boolean) {
        openPreference()
        val prefsPrivateEditor: SharedPreferences.Editor? = sharedPreferences!!.edit()
        prefsPrivateEditor!!.putBoolean(key, value)
        prefsPrivateEditor.apply()
    }

    /*For Remove variable from pref*/
    private fun remove(key: String) {
        openPreference()
        val prefsPrivateEditor = sharedPreferences!!.edit()
        prefsPrivateEditor.remove(key)
        prefsPrivateEditor.apply()
        sharedPreferences = null
    }


    /*For Remove variable from pref*/
    fun clearAllPref() {
        remove(PREF_USER_ID)
        remove(PREF_EMAIL)
        remove(PREF_LANGUAGE)
        remove(PREF_DEVICE_TOKEN)

    }

    const val PREF_USER_ID: String = "PREF_USER_ID"
    const val PREF_EMAIL: String = "PREF_EMAIL"
    const val PREF_LANGUAGE: String = "PREF_LANGUAGE"
    const val PREF_DEVICE_TOKEN: String = "PREF_DEVICE_TOKEN"


}
