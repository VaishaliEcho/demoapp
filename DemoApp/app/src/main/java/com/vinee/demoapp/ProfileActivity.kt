package com.vinee.demoapp

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.vinee.demoapp.databinding.ActivityProfileBinding

open class ProfileActivity : AppCompatActivity(){

    private lateinit var binding: ActivityProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialization()

    }


    private fun initialization() {

        binding.customHeader.setTitle(resources.getString(R.string.profile))
        binding.customHeader.hideBackButton()




    }



}