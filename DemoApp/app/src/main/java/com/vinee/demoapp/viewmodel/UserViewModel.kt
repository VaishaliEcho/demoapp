package com.vinee.demoapp.viewmodel

import android.annotation.SuppressLint
import android.content.Context
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.vinee.demoapp.model.UserModel
import com.vinee.demoapp.MainApplication
import com.vinee.demoapp.idrequest.ResponseModel
import com.vinee.demoapp.utils.DEVICE_TYPE
import com.vinee.demoapp.utils.EN
import com.vinee.demoapp.utils.Pref
import com.vinee.demoapp.utils.Util
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*


class UserViewModel() : ViewModel() {

    var loginResponse: MutableLiveData<ResponseModel<UserModel>> = MutableLiveData()


    @SuppressLint("CheckResult")
    fun loginApiRequest(
        context: Context,
        email: String,
        password: String,
    ): Observable<ResponseModel<UserModel>> {
        Util.showProgress(context)

        return Observable.create { emitter ->
            MainApplication.ipRequestService.loginRequest(
                email, password,
                DEVICE_TYPE,
                Pref.getStringValue(Pref.PREF_DEVICE_TOKEN, "").toString(),
                Pref.getStringValue(Pref.PREF_LANGUAGE, EN).toString(), TimeZone.getDefault().id
            )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {
                        if (it.body() != null)
                        {
                            Util.dismissProgress()
                            emitter.onNext(it.body()!!)
                            if (it.body()!!.success == 1) {
                                loginResponse.postValue(it.body()!!)
                            }
                        } else
                            Util.dismissProgress()
                    }, {
                        Util.dismissProgress()
                        Util.print(emitter.hashCode().toString())
                        it.printStackTrace()
                    })
        }

    }


}