package com.vinee.demoapp.idrequest

import com.vinee.demoapp.model.UserModel
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.*


interface IDRequestService {

    @FormUrlEncoded
    @POST(IDRequestBuilder.LOGIN_API) //1
    fun loginRequest(
        @Field("email") email: String,
        @Field("password") password: String,
        @Field("device_type") deviceType: String, // 1=Android 2=Ios
        @Field("device_token") deviceToken: String,
        @Field("language") language: String,
        @Field("timezone") timezone: String
    ): Single<Response<ResponseModel<UserModel>>>




}

