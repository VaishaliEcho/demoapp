package com.vinee.demoapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.vinee.demoapp.databinding.RawNotificationBinding
import com.vinee.demoapp.model.NotificationModel


class NotificationListAdapter(var context: Context) :
    RecyclerView.Adapter<NotificationListAdapter.Holder>() {

    var objList: ArrayList<NotificationModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {

        return Holder(RawNotificationBinding.inflate(LayoutInflater.from(context), parent, false))
    }

    override fun getItemCount(): Int {
        return objList.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {

        holder.bind(position)
    }


    inner class Holder(mBinding: RawNotificationBinding) : RecyclerView.ViewHolder(mBinding.root) {

        var binding = mBinding

        fun bind(position: Int) {

            binding.lblTitle.text = objList[position].message
            binding.lblCreatedAt.text = objList[position].created

        }
    }

    fun addData(mObjList: ArrayList<NotificationModel>) {
        objList = ArrayList()
        objList.addAll(mObjList)
        this.notifyDataSetChanged()
    }


}