package com.vinee.demoapp

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.vinee.demoapp.databinding.ActivityLoginBinding
import com.vinee.demoapp.utils.Pref
import com.vinee.demoapp.utils.Util
import com.vinee.demoapp.viewmodel.UserViewModel
import com.vinee.demoapp.viewmodel.ViewModelFactory

open class LoginActivity : AppCompatActivity(), View.OnClickListener {

    private var runnable: Runnable? = null
    private var handler: Handler? = null
    private lateinit var binding: ActivityLoginBinding
    private lateinit var userViewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        window.statusBarColor = ContextCompat.getColor(this@LoginActivity, R.color.sky_blue)

        initialization()

    }


    private fun initialization() {
        manageApiResponse()
        binding.btnLogin.setOnClickListener(this)

    }

    override fun onDestroy() {
        super.onDestroy()
        if (handler != null && runnable != null)
            handler!!.removeCallbacks(runnable!!)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogin -> {

                if(isValidate()){
                    Pref.setStringValue(Pref.PREF_USER_ID,"1")  // Temp Login User
                    Pref.setStringValue(Pref.PREF_EMAIL,Util.getTextValue(binding.edtEmail))  // Temp Login User
                    startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
                    finishAffinity()
                }

                /*For Make App Functional*/
//                callApi()



            }
        }
    }



    private fun isValidate(): Boolean {
        var isError = true
        when {
            Util.isEmptyText(binding.edtEmail) -> {
                isError = false
                binding.edtEmail.requestFocus()
                Util.showToastMessage(
                    this,
                    String.format(
                        resources.getString(R.string.please_enter_value_validation),
                        resources.getString(R.string.email_address)
                    ),
                    true
                )
            }
            !Util.isValidEmail(Util.getTextValue(binding.edtEmail)) -> {
                isError = false
                binding.edtEmail.requestFocus()
                Util.showToastMessage(
                    this,
                    String.format(
                        resources.getString(R.string.please_enter_valid_value_validation),
                        resources.getString(R.string.email_address)
                    ),
                    true
                )
            }
            Util.isEmptyText(binding.edtPassword) -> {
                isError = false
                binding.edtPassword.requestFocus()
                Util.showToastMessage(
                    this,
                    String.format(
                        resources.getString(R.string.please_enter_value_validation),
                        resources.getString(R.string.password)
                    ),
                    true
                )
            }
            Util.getTextValue(binding.edtPassword).length < 6 -> {
                isError = false
                binding.edtPassword.requestFocus()
                Util.showToastMessage(
                    this,
                    resources.getString(R.string.password_should_be_six_character),
                    true
                )
            }
        }
        return isError
    }



    @SuppressLint("CheckResult")
    private fun callApi() {
        if (Util.isOnline(this@LoginActivity)) {
                    userViewModel.loginApiRequest(this@LoginActivity,Util.getTextValue(binding.edtEmail),Util.getTextValue(binding.edtPassword))
        } else {
            Util.showToastMessage(this@LoginActivity, resources.getString(R.string.check_internet), false)
        }
    }

    private fun manageApiResponse() {

        userViewModel = ViewModelProvider(
            this,
            ViewModelFactory.viewModelFactory
        ).get(UserViewModel::class.java)

        /*Login Api Response*/

        userViewModel.loginResponse.observe(this, {
            if(it.success==1) {
                startActivity(Intent(this@LoginActivity, HomeActivity::class.java))
            }else{
                Util.showToastMessage(this@LoginActivity, it.msg, true)
            }

        })


    }


}