package com.vinee.demoapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class NotificationModel : Serializable {


    @Expose
    @SerializedName("message")
    var message: String = ""

    @Expose
    @SerializedName("user_id")
    var userId: String = ""

    @Expose
    @SerializedName("active")
    var active: String = ""

    @Expose
    @SerializedName("notification_id")
    var notificationId: String = ""

    @Expose
    @SerializedName("created")
    var created: String = ""

    @Expose
    @SerializedName("description")
    var description: String = ""
}
