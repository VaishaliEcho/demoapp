package com.vinee.demoapp

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.vinee.demoapp.databinding.ActivitySettingBinding
import com.vinee.demoapp.utils.Pref

open class SettingsActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var binding: ActivitySettingBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialization()

    }


    private fun initialization() {

        binding.customHeader.setTitle(resources.getString(R.string.settings))
        binding.btnLogOut.setOnClickListener(this)
        binding.customHeader.hideBackButton()
        binding.txtUserEmail.text = Pref.getStringValue(Pref.PREF_EMAIL,"")

    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.btnLogOut -> {
                Pref.clearAllPref()
                startActivity(Intent(this@SettingsActivity, LoginActivity::class.java))
                finishAffinity()
            }
        }
    }

}