package com.vinee.demoapp

import android.app.Application
import com.vinee.demoapp.idrequest.IDRequestService
import com.vinee.demoapp.idrequest.IDRequestBuilder

class MainApplication : Application() {
    companion object {
        lateinit var instance: MainApplication
        val ipRequestService: IDRequestService = IDRequestBuilder.getRequestBuilder()
    }


    override fun onCreate() {
        super.onCreate()
//All Application Level initialization goes here
//        Fresco.initialize(this)

        instance = this

    }

}