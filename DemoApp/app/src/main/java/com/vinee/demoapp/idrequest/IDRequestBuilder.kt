package com.vinee.demoapp.idrequest

import com.vinee.demoapp.BuildConfig
import com.vinee.demoapp.utils.BASE_URL
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object IDRequestBuilder {

    private const val NETWORK_CALL_TIMEOUT = 30
    private const val API_URL: String = BASE_URL + "api/"

    const val LOGIN_API: String = "login" // 1.

    private  var ipRequestService: IDRequestService?=null

    fun getRequestBuilder(): IDRequestService {
        if(ipRequestService ==null)
            ipRequestService = create()
        return ipRequestService!!
    }


    private fun create(): IDRequestService {
        return Retrofit.Builder()
            .baseUrl(API_URL)
            .client(
                OkHttpClient.Builder()
                    .addInterceptor(
                        HttpLoggingInterceptor().setLevel(
                            HttpLoggingInterceptor.Level.BODY ))
                    .readTimeout(
                        NETWORK_CALL_TIMEOUT.toLong(),
                        TimeUnit.SECONDS
                    )
                    .writeTimeout(
                        NETWORK_CALL_TIMEOUT.toLong(),
                        TimeUnit.SECONDS
                    )
                    .build()
            )
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(IDRequestService::class.java)
    }

}