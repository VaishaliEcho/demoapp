package com.vinee.demoapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserModel : Serializable {

    @Expose
    @SerializedName("user_id")
    var userId: String = ""

    @Expose
    @SerializedName("email")
    var email: String = ""


    @Expose
    @SerializedName("profile_pic")
    var profilePic: String = ""


    @Expose
    @SerializedName("user_name")
    var userName: String = ""


    @Expose
    @SerializedName("first_name")
    var firstName: String = ""

    @Expose
    @SerializedName("last_name")
    var lastName: String = ""

    @Expose
    @SerializedName("address")
    var address: String = ""

    var password: String = ""




}