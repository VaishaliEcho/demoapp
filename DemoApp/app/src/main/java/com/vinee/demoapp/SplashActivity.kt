package com.vinee.demoapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.vinee.demoapp.databinding.ActivitySplashBinding
import com.vinee.demoapp.utils.Pref

open class SplashActivity : AppCompatActivity() {

    private var runnable: Runnable? = null
    private var handler: Handler? = null
    private lateinit var binding: ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initialization()

    }


    private fun initialization() {


        handler = Handler(Looper.getMainLooper())
        runnable = Runnable {
/*After 2 second call this */
            if (Pref.getStringValue(Pref.PREF_USER_ID, "").toString().isEmpty())
                startActivity(Intent(this@SplashActivity, LoginActivity::class.java))
            else
                startActivity(Intent(this@SplashActivity, HomeActivity::class.java))
            finishAffinity()
        }
/* Wait for 2 second then lunch Screen*/
        handler!!.postDelayed(runnable!!, 2000)
    }

    override fun onDestroy() {
        super.onDestroy()
        if (handler != null && runnable != null)
            handler!!.removeCallbacks(runnable!!)
    }

}