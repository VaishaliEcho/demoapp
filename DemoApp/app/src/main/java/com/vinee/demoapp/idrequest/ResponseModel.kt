package com.vinee.demoapp.idrequest

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.util.*

class ResponseModel<T> {

    @Expose
    @SerializedName("success")
    var success: Int = 0

    @Expose
    @SerializedName("msg")
    var msg: String = ""

    @Expose
    @SerializedName("result")
    var result: ArrayList<T> = ArrayList()


}
