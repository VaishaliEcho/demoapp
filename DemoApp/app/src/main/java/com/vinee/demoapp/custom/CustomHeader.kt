package com.vinee.demoapp.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import com.vinee.demoapp.databinding.CustomHeaderBinding

class CustomHeader @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) :
    RelativeLayout(context, attrs, defStyleAttr) {
    private var binding: CustomHeaderBinding =
        CustomHeaderBinding.inflate(LayoutInflater.from(context), this, true)

    init {
        binding.btnBack.setOnClickListener {
            (context as AppCompatActivity).finish()
        }
    }


    public fun setTitle(title: String) {
        binding.txtHeaderTitle.text = title
    }

    public fun hideBackButton() {
        binding.btnBack.visibility = View.INVISIBLE
    }

    public fun setRightDrawable(drawable: Int?) {
        binding.btnRight.visibility = View.VISIBLE
        if (drawable != null)
            binding.btnRight.setImageResource(drawable)
    }

    public fun get(): CustomHeaderBinding {
        return binding
    }


}