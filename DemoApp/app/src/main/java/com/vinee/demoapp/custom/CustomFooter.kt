package com.vinee.demoapp.custom

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import com.vinee.demoapp.HomeActivity
import com.vinee.demoapp.ProfileActivity
import com.vinee.demoapp.R
import com.vinee.demoapp.SettingsActivity
import com.vinee.demoapp.databinding.CustomFooterBinding

class CustomFooter @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    RelativeLayout(context, attrs, defStyleAttr), View.OnClickListener {


    private var binding: CustomFooterBinding =
        CustomFooterBinding.inflate(LayoutInflater.from(context), this, true)

    init {


        binding.btnSettings.setOnClickListener(this);
        binding.btnProfile.setOnClickListener(this);
        binding.btnHome.setOnClickListener(this);


    }

    override fun onClick(v: View?) {
        when(v!!.id)
        {
            R.id.btnProfile ->{
                if (context !is ProfileActivity) {
                    context.startActivity(Intent(context, ProfileActivity::class.java))
                    (context as Activity).finishAffinity()
                }
            }
            R.id.btnHome ->{
                if (context !is HomeActivity) {
                    context.startActivity(Intent(context, HomeActivity::class.java))
                    (context as Activity).finishAffinity()
                }
            }
            R.id.btnSettings ->{

                if (context !is SettingsActivity) {
                    context.startActivity(Intent(context, SettingsActivity::class.java))
                    (context as Activity).finishAffinity()
                }
            }

        }
    }


}