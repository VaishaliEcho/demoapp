package com.vinee.demoapp.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.viewbinding.BuildConfig
import com.vinee.demoapp.custom.CustomDialog

object Util {

    @SuppressLint("StaticFieldLeak")
    private var customDialog: CustomDialog? = null

    /*Debug Code : Use for Print log*/
    fun print(message: String) {
        if (BuildConfig.DEBUG) {

            if (message.length > 1000) {
                val maxLogSize = 1000
                for (i in 0..message.length / maxLogSize) {
                    val start = i * maxLogSize
                    var end = (i + 1) * maxLogSize
                    end = if (end > message.length) message.length else end
                    Log.v("Print ::", message.substring(start, end))
                }
            } else {
                Log.i("Print ::", message)
            }
        }
    }

    /*Show Custom Progress*/
    fun showProgress(context: Context) {
        try {
            if (customDialog != null && customDialog!!.isShowing)
                customDialog!!.dismiss()

            customDialog = CustomDialog(context)
            customDialog!!.setCancelable(false)
            customDialog!!.show()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    /*Hide Custom Progress*/
    fun dismissProgress() {
        if (customDialog !=null && customDialog!!.isShowing)
            customDialog!!.dismiss()
        customDialog = null
    }

    /*Check Net availability*/
    fun isOnline(context: Context): Boolean {
        return try {
            val connectivityManager = context
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val n = connectivityManager.activeNetwork
                if (n != null) {
                    val nc = connectivityManager.getNetworkCapabilities(n)
                    return nc!!.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) || nc.hasTransport(
                        NetworkCapabilities.TRANSPORT_WIFI
                    )
                }
                return false
            } else {
                @Suppress("DEPRECATION")
                val netInfo = connectivityManager.activeNetworkInfo
                @Suppress("DEPRECATION")
                return netInfo != null && netInfo.isConnectedOrConnecting
            }


        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

/*Display toast*/
    fun showToastMessage(context: Context, mgs: String, isShort: Boolean) {
        Toast.makeText(context, mgs, if (isShort) Toast.LENGTH_SHORT else Toast.LENGTH_LONG).show()
    }


    /*Fetch Text from text with trim*/
    fun getTextValue(view: View): String {
        return (view as? EditText)?.text?.toString()?.trim { it <= ' ' }
            ?: ((view as? TextView)?.text?.toString()?.trim { it <= ' ' }
                ?: "")

    }

    /*Validate Email*/
    fun isValidEmail(target: CharSequence): Boolean {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target)
            .matches()
    }

    fun isEmptyText(view: View?): Boolean {
        return if (view == null)
            true
        else
            getTextValue(view).isEmpty()


    }

}