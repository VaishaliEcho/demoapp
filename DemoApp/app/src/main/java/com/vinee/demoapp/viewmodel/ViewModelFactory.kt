package com.vinee.demoapp.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider


@Suppress("UNCHECKED_CAST")
class ViewModelFactory() :
    ViewModelProvider.NewInstanceFactory() {

    companion object {
        val viewModelFactory: ViewModelFactory = ViewModelFactory()
    }

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        when {

            modelClass.isAssignableFrom(UserViewModel::class.java) -> {
                return UserViewModel() as T
            }
//            modelClass.isAssignableFrom(LookingForViewModel::class.java) -> {
//                return LookingForViewModel() as T
//            }

        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }

}